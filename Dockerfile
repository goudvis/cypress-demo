FROM gcr.io/google-appengine/python
LABEL python_version=python3.6
RUN virtualenv --no-download /env -p python3.6
ENV VIRTUAL_ENV /env
ENV PATH /env/bin:$PATH
ADD requirements.txt /app/
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

# Environment variable - make stderr/stdout unbuffered
ENV PYTHONUNBUFFERED 1

# Copy code into container
ADD . /app/

# EXPOSE port 8000 to allow communication to/from server
EXPOSE 8080

RUN chmod +x gunicornrun.sh
CMD ["./gunicornrun.sh"]
