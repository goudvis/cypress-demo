from django.test import TestCase, RequestFactory
from .. import views, models


class DetailViewTest(TestCase):

    fixtures = [
        'polls/tests/poll.json'
    ]

    def test_vote(self):
        self.assertEqual(0, models.Choice.objects.get(pk=1).votes)
        request = RequestFactory().post('/1/vote/', data={'choice': 1})
        views.vote(request, 1)
        self.assertEqual(1, models.Choice.objects.get(pk=1).votes)
