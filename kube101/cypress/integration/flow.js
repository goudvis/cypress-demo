/* global describe it cy expect */

describe('Poll Test', function() {
    it('Tests the poll system flow', function() {
        cy.visit('/');

        cy.get('ul>li>a:first').click();
        cy.location().should(function(loc) { expect(loc.pathname).to.eq('/1/'); });

        cy.get('form>label[for=choice1]').click();
        cy.get('form>input[type=submit]').click();
        cy.location().should(function(loc) { expect(loc.pathname).to.eq('/1/results/'); });

        cy.get('li:first-child').contains(/Yes, I love beer! -- \d+ votes?/)
    });
});