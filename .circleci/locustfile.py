from locust import HttpLocust, TaskSet, task
from requests import Response
import re


class UserBehavior(TaskSet):
    def on_start(self):
        """ on_start is called when a Locust start before any task is scheduled """
        self.client.headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.3',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Accept-Language': 'en-us',
            'Accept-Encoding': 'gzip, deflate',
            'Connection': 'Keep-Alive',
            'Cache-Control': 'no-cache',
            'Upgrade-Insecure-Requests': '1',
            'Pragma': 'no-cache',
        }

    def get_csrf_token(self, response: Response):
        match = re.search('name="csrfmiddlewaretoken" value="([^"]+)"', response.text)
        if not match:
            return ''
        return match.group(1)

        pass

    def client_get(self, url, *args, **kwargs):
        response = self.client.get(url, *args, **kwargs)
        referer = response.request.url
        token = self.get_csrf_token(response)
        return response, referer, token

    @task(1)
    def flow(self):
        # fetch the home page
        self.client.get("/")

        # fetch the voting page for question 1
        reponse, referer, token = self.client_get("/1/")  # fetch vote page

        # post to the voting page
        with self.client.post(
                "/1/vote/",
                data={'choice': '1', 'csrfmiddlewaretoken': token},
                headers={'Referer': referer},
                catch_response=True,
                allow_redirects=False
        ) as response:
            if not response.is_redirect:
                response.failure(f"redirect status expected, got {response.status_code} instead")
            elif response.headers.get('Location') != "/1/results/":
                response.failure("redirect to /1/results/ expected")

        # fetch the result page
        self.client.get("/1/results/")


class WebsiteUser(HttpLocust):
    host = 'http://127.0.0.1:8000'  # override by cli by providing --host
    task_set = UserBehavior
